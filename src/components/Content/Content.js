import React from 'react';
import axios from 'axios';

class Content extends React.PureComponent {
  state = {
    title: '',
    content: ''
  };

  getContent = () => {
    axios.get(`https://best-work-65.firebaseio.com/${this.props.match.params.page}.json`).then(res => {
      this.setState({title: res.data.title, content: res.data.content});
      console.log(res);
    })
  };

  componentDidUpdate (prevProps) {
    if (this.props.match.params.page !== prevProps.match.params.page)
      this.getContent();
  }

  componentDidMount () {
    this.getContent();
  }

  render() {
    return (
      <div>
        <h2>{this.state.title}</h2>
        <div dangerouslySetInnerHTML={{__html: this.state.content}} />
      </div>
    );
  }
}

export default Content;