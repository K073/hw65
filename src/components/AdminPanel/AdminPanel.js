import React from 'react';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import {EditorState, convertToRaw, ContentState} from 'draft-js';
import {Editor} from "react-draft-wysiwyg";
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';
import axios from 'axios';

class AdminPanel extends React.Component {
  state = {
    editorState: EditorState.createEmpty(),
    pathNames: [],
    currentPath: '',
    title: ''
  };

  onEditorStateChange = (editorState) => {
    this.setState({
      editorState,
    });
  };

  getDataWithPath = path => {
    axios.get(`https://best-work-65.firebaseio.com/${path}.json`)
      .then(res => {
        const title = res.data.title
        const contentState = ContentState.createFromBlockArray(htmlToDraft(res.data.content));
        const editorState = EditorState.createWithContent(contentState);
        this.setState({editorState});
        this.setState({title});
      })
  };

  selectHandler = e => {
    const value = e.target.value;
    this.getDataWithPath(value);
    this.setState({[e.target.name]: value})
  };

  inputHandler = e => {
    const value = e.target.value;
    this.setState({[e.target.name]: value})
  };

  sendData = () => {
    axios.put(`https://best-work-65.firebaseio.com/${this.state.currentPath}.json`, {title: this.state.title, content: draftToHtml(convertToRaw(this.state.editorState.getCurrentContent()))})
      .then(res => {
        console.log(res);
      })
  };

  componentDidMount () {
    axios.get(`https://best-work-65.firebaseio.com/.json`).then(res => {
        const pathNames = Object.keys(res.data ? res.data : {});
        this.getDataWithPath(pathNames[0]);
        this.setState({pathNames});
        this.setState({currentPath: pathNames[0]})
      })
  }
  render() {
    const {editorState} = this.state;
    return (
      <div>
        <select onChange={this.selectHandler} name='currentPath' value={this.state.currentPath}>
          {this.state.pathNames.map(value => <option key={value} value={value}>{value}</option>)}
        </select>
        <input name='title' value={this.state.title} onChange={this.inputHandler} type="text"/>
        <Editor
          editorState={editorState}
          wrapperClassName="demo-wrapper"
          editorClassName="demo-editor"
          onEditorStateChange={this.onEditorStateChange}
        />
        <button onClick={this.sendData}>SAVE</button>
      </div>
    );
  }
}

export default AdminPanel;