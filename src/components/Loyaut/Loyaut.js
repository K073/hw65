import React from 'react';
import {Link} from "react-router-dom";

const Layout = props => (
  <div>
    <nav>
      <Link to='/about'>About page</Link>
      <Link to='/contact'>Contact page</Link>
      <Link to='/tada'>Tada page</Link>
      <Link to='/home'>Home page</Link>
      <Link to='/admin'>Admin</Link>
    </nav>
    <div className='container'>
      {props.children}
    </div>
  </div>
);

export default Layout;