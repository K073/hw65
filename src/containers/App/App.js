import React, { Component } from 'react';
import './App.css';
import {BrowserRouter, Route, Switch} from "react-router-dom";
import Content from "../../components/Content/Content";
import AdminPanel from "../../components/AdminPanel/AdminPanel";
import Layout from "../../components/Loyaut/Loyaut";

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Layout>
          <Switch>
            <Route path='/admin' axact component={AdminPanel} />
            <Route path='/:page' component={Content}/>
          </Switch>
        </Layout>
      </BrowserRouter>
    );
  }
}

export default App;
